#!/usr/bin/python
"""
This module contains the main class definition for the scanner and a class
Token that contains information about tokens.

Attributes:
    reswords (dict):  reserved words and their corresponding tokens.
    operators (dict):  word operators and operator type.
"""
import string
import logging
logger = logging.getLogger('compiler.scanner')

reswords = {
    "begin" : "begint",
    "program" : "programt",
    "const" : "constt",
    "var" : "vart",
    "procedure" : "proceduret",
    "if" : "ift",
    "while" : "whilet",
    "do" : "dot",
    "then" : "thent",
    "else" : "elset",
    "real" : "realt",
    "integer" : "integert",
    "boolean" : "booleant",
    "char" : "chart",
    "array" : "arrayt",
    "function" : "functiont",
    "end" : "endt",
    "not" : "nott",
    "read" : "readt",
    "write" : "writet",
    "writeln" : "writet"
}

operators = {
    "or" : "addop",
    "div" : "mulop",
    "mod" : "mulop",
    "and" : "mulop"
}


class Scanner(object):
    """ The class that does lexical analysis and converts a stream of characters
        from a file to a stream of tokens

        Attributes:
            ch(str) : single character string that contains the current
                      character being inspected
            lexeme(str) : the lexeme built from a string of characters
            token(str) : the token corresponding to the lexeme in question
            value(number) : the integer/real value of a num token
            literal(str) : literal string token
            i(int) : index to keep track of input data

    """
    def __init__(self, filename=None):
        """
            Class contructor

            Args:
                filename(str) : path of file to be opened
        """
        if filename:
            self.readfile(filename)
        self.lexeme = ""
        self.i = 0
        self.linenumber = 1

    def readfile(self, filename):
        """
            Attempts to open file and load stream into memory.

            Args:
                filename(str) : path of file to be opened
        """
        try:
            if filename[-4:] != '.pas':
                raise Exception("Unexpected file type.")
            file = open(filename)
            self.data = file.read()
            file.close()
        except IOError:
            self.print_error("No such file.")

    def getchar(self):
        """
            Gets next character from stream. Stores it in self.ch
        """
        self.ch = self.data[self.i]
        if self.ch == "\n":
            self.linenumber += 1
        self.i += 1

    def print_error(self, errstring):
        """
            Utility method to print pretty error messages.
        """
        logger.error(errstring)
        exit(1)

    def get_next_token(self):
        """
            The main method used by other modules. Reads input stream and
            identifies the next token. The token is stored in self.token after
            this executes.

            Returns: token identified
        """
        try:
            self.getchar()
            while self.ch in (string.whitespace + "{"):
                if self.ch == "{":
                    self.getchar()
                    self.process_comment()
                self.getchar()
            self.process_token()
        except IndexError:
            self.token = Token("eoft")
        return self.token

    def process_token(self):
        """
            Inspects the first character of the incoming token and passes on
            to specifc methods.
        """
        self.lexeme = self.ch
        try:
            self.getchar()
        except IndexError:
            self.ch = ""
            self.i += 1

        # proceed with valid tokens
        if self.lexeme.isalpha():
            self.process_word_token()
        elif self.lexeme.isdigit():
            self.process_num_token()
        elif self.lexeme == "'":
            self.process_string_literal()
        elif self.lexeme in ":<>":
            self.process_double_token()
        else:
            self.process_single_token()
        self.i -= 1
        if self.ch == "\n":
            self.linenumber -= 1
        self.token.lexeme = self.lexeme
        self.token.linenumber = self.linenumber
        return self.token

    def process_string_literal(self):
        """
            Processes a string literal. Stores literal in self.literal
        """
        while True:
            if self.ch == "'":
                self.lexeme += self.ch
                try:
                    self.getchar()
                except IndexError:
                    self.ch = ""
                    self.i += 1
                break
            else:
                self.lexeme += self.ch
            try:
                self.getchar()
            except IndexError:
                self.print_error("End of file in between string literal.")
            if self.ch in "\n":
                self.print_error("String can only be in a single line.")

        self.token = Token("literalt")
        self.token.literal = self.lexeme
        self.lexeme = None

    def process_comment(self):
        """
            Processes comments.
        """
        while self.ch != "}":
            if self.ch == "{":
                self.print_error("Comments may not contain '{'.")
            try:
                self.getchar()
            except IndexError:
                self.print_error("End of file before comment closed.")
        self.lexeme = None

    def process_num_token(self):
        """
            Processes integers and real numbers. Stores value in self.value
        """
        decimal_count = 0
        try:
            while self.ch.isdigit() or self.ch == ".":
                if self.ch == ".":
                    decimal_count += 1
                if decimal_count > 1:
                    break
                self.lexeme += self.ch
                self.getchar()
        except IndexError:
            self.ch = ""
            self.i += 1
        if self.lexeme.endswith("."):
            self.token = Token("unknownt")
        else:
            self.token = Token("numt")
            try:
                self.token.value = int(self.lexeme)
            except ValueError:
                self.token.value = float(self.lexeme)

    def process_word_token(self):
        """
            Proceeses words. Tries to identify them as reserved words or
            operators. Otherwise classifies them as idt. Stores lexeme in
            self.lexeme
        """
        # read the whole lexeme
        count = 1
        try:
            while self.ch.isalnum() and len(self.ch) > 0:
                self.lexeme += self.ch
                self.getchar()
                count += 1
        except IndexError:
            self.ch = ""
            self.i += 1
        if count > 15:
            self.print_error("Words over 15 characters in length are not allowed. Belligerent word: {0}"
                             .format(self.lexeme))
        # categorize the lexeme
        if self.get_reserved_word_token(self.lexeme):
            self.token = Token(reswords.get(self.lexeme.lower()))
        elif self.get_operator_token(self.lexeme):
            self.token = Token(operators.get(self.lexeme.lower()))
        else:
            self.token = Token("idt")

    def get_reserved_word_token(self, word):
        """
            Look up reserved word
        """
        return reswords.get(word.lower())

    def get_operator_token(self, word):
        """
            Lookip operator word
        """
        return operators.get(word.lower())

    def process_double_token(self):
        """
            Proceeses two character symbols.
        """
        double_tokens = {
            ":=" : "assignop",
            "<=" : "relop",
            ">=" : "relop",
            "<>" : "relop"
        }
        peek = self.lexeme + self.ch
        if peek in double_tokens:
            self.lexeme += self.ch
            self.token = Token(double_tokens[peek])
            self.token.symbol = self.lexeme
            try:
                self.getchar()
            except IndexError:
                self.ch = ""
                self.i += 1
        else:
            self.process_single_token()

    def process_single_token(self):
        """
            Processes single character symbols.
        """
        single_tokens = {
            "=" : "relop",
            "<" : "relop",
            ">" : "relop",
            "+" : "addop",
            "-" : "addop",
            "*" : "mulop",
            "/" : "mulop",
            "(" : "lparen",
            ")" : "rparen",
            "," : "comma",
            ";" : "semicolon",
            "." : "period",
            ":" : "colon"
        }
        if self.lexeme in single_tokens:
            self.token = Token(single_tokens[self.lexeme])
            self.token.symbol = self.lexeme
        else:
            self.token = Token("unknownt")


class Token(object):
    """
        Defines the attributes of tokens.

        Attributes:
            type(str):      Token type
            lexeme(str):    Lexeme of token
            value(num):     Number value
            literal(str):   String literal
            symbol(str):    Symbol of token
    """

    def __init__(self, token=None):
        self.token = token
        self.lexeme = None
        self.value = None
        self.literal = None
        self.symbol = None
        self.linenumber = None

    def __eq__(self, other):
        if isinstance(other, Token):
            return self.token == other.token
        elif isinstance(other, basestring):
            return self.token == other
        return NotImplemented

    def __str__(self):
        return str(dict((k, vars(self)[k])
                        for k in vars(self).keys() if vars(self)[k]))

    def __repr__(self):
        return str(dict((k, vars(self)[k])
                        for k in vars(self).keys() if vars(self)[k]))

    def display_output(self, debug=False):
        """
            Pretty print current token and attributes.
        """
        output = "Line Number: {0}".format(self.linenumber)
        if self.token:
            output += "  Token: {0}".format(self.token)
        if self.lexeme:
            output += "  Lexeme: {0}".format(self.lexeme)
        if self.symbol:
            output += "  Symbol: {0}".format(self.symbol)
        if self.value:
            output += "  Value: {0}".format(self.value)
        if self.literal:
            output += "  Literal: {0}".format(self.literal)
        if output:
            if debug:
                logger.debug(output)
            else:
                logger.info(output)
