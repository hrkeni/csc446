#!/usr/bin/python
"""
    This module contains the class definitions for the symbol table and the
    symbol table entries
"""

import logging
import pprint
from collections import defaultdict
from scanner import Token

logger = logging.getLogger('compiler.symbol_table')
pp = pprint.PrettyPrinter()


class SymbolTable(object):
    """
        This class maintains a symbol table for the compiler. It uses a
        dictionary (hash table) to keep track of symbol table entries.

        Attributes:
            table(dict) : the hash table

    """

    def __init__(self):
        """
            Class constructor. Initializes table.
        """
        self.table = defaultdict(list)
        self.temp = 0
        self.literals = []

    def insert(self, token, depth, entry_type):
        """
            Insert a token of entry_type into table at given depth

            Args:
                token(Token)    : the token
                depth(int)      : the depth
                entry_type(str) : the type (variable, constant, procedure)

            Returns:
                Entry : the entry inserted

            Raises:
                DuplicateInsertion : if the inserted token with same depth
                                     already exists
                TypeError          : if invalid type
        """
        if entry_type == "variable":
            entry = VariableEntry(token=token, depth=depth)
        elif entry_type == "constant":
            entry = ConstantEntry(token=token, depth=depth)
        elif entry_type == "procedure":
            entry = ProcedureEntry(token=token, depth=depth)
        else:
            raise TypeError("Unknown type")
        dup = self.find_at_depth(token.lexeme, depth)
        if dup:
            logger.error("insert: Duplicate found: {0}".format(dup))
            raise DuplicateInsertion("Entry with lexeme {0} (line: {1}) at depth {2} already exists".format(token.lexeme, token.linenumer, depth))
        self.table[token.lexeme].append(entry)
        return entry

    def lookup(self, lexeme):
        """
            Looksup the entry for given lexeme

            Args:
                lexeme(str) : the lexeme

            Returns:
                Entry : latest inserted entry
        """
        if lexeme in self.table:
            entry = self.table[lexeme][-1]
            logger.debug("lookup: found entry: {0}".format(entry))
            return entry
        return None

    def delete(self, depth):
        """
            Deletes all entries at given depth

            Args:
                depth(int) : the depth
        """
        deleting = []
        for lexeme, entries in self.table.iteritems():
            deleting = deleting + [e for e in entries if e.depth == depth]
            # filter through the entries and keep only if depth is eq
            entries[:] = [e for e in entries if e.depth != depth]
        for k in self.table.keys():
            if len(self.table[k]) == 0:
                del self.table[k]
        logger.debug("Deleting: {0}".format(deleting))

    def all_at_depth(self, depth):
        """
            Returns all entries at given depth

            Args:
                depth(int) : the depth
        """
        result = []
        for lexeme, entries in self.table.iteritems():
            result += [{lexeme: e} for e in entries if e.depth == depth]
        return result

    def find_at_depth(self, lexeme, depth):
        """
            Looksup entry at depth

            Args:
                lexeme(str) : the lexeme
                depth(int)  : the depth

            Returns:
                Entry: the entry
        """
        if lexeme in self.table:
            for entry in self.table[lexeme]:
                if entry.depth == depth:
                    logger.debug("find_at_depth: Found {0}".format(entry))
                    return entry
        return None

    def gentemp(self, depth, proc):
        """
            Generates and returns a temporary symbol table entry name.

            Returns:
                str : temp name
        """
        self.temp = self.temp + 1
        tempstr = '_t{0}'.format(self.temp)
        temptok = Token('idt')
        temptok.lexeme = tempstr
        entry = self.insert(temptok, depth, 'variable')
        if proc:
            entry.offset = proc.size_local
            entry.vartype = 'integer'
            entry.local = True
            proc.size_local += 2
        return entry

    def __str__(self):
        return pp.pformat(dict(self.table))

    def __repr__(self):
        return pp.pformat(self.table)


class Entry(object):
    """
        A symbol table entry

        Attributes:
            token(Token) : the token for the entry
            depth(int)   : depth of the entry
    """

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def entry_type(self):
        return "entry"

    def __str__(self):
        out = vars(self)
        out['type'] = self.entry_type()
        return str(out)

    def __repr__(self):
        out = vars(self)
        out['type'] = self.entry_type()
        return str(out)


class VariableEntry(Entry):
    """
        A symbol table entry for a variable

        Attributes:
            vartype(str) : type of the variable (bool, char, int, real)
            size(int)    : size of the variable
            offset(int)  : memory offset of the variable
            local(bool)  : is local
            param(bool)  : is a parameter
    """
    def __init__(self, **kwargs):
        self.local = False
        self.param = False
        self.type = 'variable'
        super(VariableEntry, self).__init__(**kwargs)

    def entry_type(self):
        return "variable"


class ConstantEntry(Entry):
    """
        A symbol table entry for a constant

        Attributes:
            consttype(str) : type of the constant (int or real)
            value(num)     : value of the constant
            offset(int)    : memory offset of the constant
    """
    def __init__(self, **kwargs):
        self.type = 'constant'
        super(ConstantEntry, self).__init__(**kwargs)

    def entry_type(self):
        return "constant"


class ProcedureEntry(Entry):
    """
        A symbol table entry for a procedure

        Attributes:
            size(int)        : Size of local
            num_pars(int)    : Number of parameters
            return_type(str) : return type (int, real, bool, char)
            param_list(list) : list of parameters
    """
    def __init__(self, **kwargs):
        self.type = 'procedure'
        super(ProcedureEntry, self).__init__(**kwargs)

    def entry_type(self):
        return "procedure"


class DuplicateInsertion(Exception):
    pass
