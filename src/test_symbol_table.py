#!/usr/bin/python
"""
Test suite for symbol table
"""
from symbol_table import SymbolTable, DuplicateInsertion
from scanner import Token
import unittest


class TestSymbolTable(unittest.TestCase):

    def setUp(self):
        self.st = SymbolTable()

    def test_insert(self):
        t = Token("idt")
        t.lexeme = "hello"
        self.st.insert(t, 1, "variable")
        self.assertEqual(len(self.st.table["hello"]), 1)
        self.assertEqual(self.st.table["hello"][-1].token, t)
        self.assertEqual(self.st.table["hello"][-1].depth, 1)

        t2 = Token("idt")
        t2.lexeme = "hello"
        self.st.insert(t2, 2, "variable")
        self.assertEqual(len(self.st.table["hello"]), 2)
        self.assertEqual(self.st.table["hello"][-1].token, t2)
        self.assertEqual(self.st.table["hello"][-1].depth, 2)

        t3 = Token("idt")
        t3.lexeme = "world"
        self.st.insert(t3, 2, "variable")
        self.assertEqual(len(self.st.table["world"]), 1)
        self.assertEqual(self.st.table["world"][-1].token, t3)
        self.assertEqual(self.st.table["world"][-1].depth, 2)

    def test_insert_duplicate(self):
        t = Token("idt")
        t.lexeme = "hello"
        self.st.insert(t, 1, "variable")

        t2 = Token("idt")
        t2.lexeme = "hello"
        try:
            # should raise a DuplicateInsertion here
            self.st.insert(t2, 1, "variable")
            self.assertEqual(True, False)
        except DuplicateInsertion:
            pass

    def test_lookup_exists(self):
        t = Token("idt")
        t.lexeme = "hello"
        self.st.insert(t, 1, "variable")
        entry = self.st.lookup("hello")
        self.assertEqual(entry.token, t)
        self.assertEqual(entry.depth, 1)

    def test_lookup_not_exists(self):
        entry = self.st.lookup("hello")
        self.assertEqual(entry, None)

    def test_delete(self):
        t = Token("idt")
        t.lexeme = "hello"
        self.st.insert(t, 1, "variable")
        self.assertEqual(len(self.st.table["hello"]), 1)
        self.assertEqual(self.st.table["hello"][-1].token, t)
        self.assertEqual(self.st.table["hello"][-1].depth, 1)

        t2 = Token("idt")
        t2.lexeme = "hello"
        self.st.insert(t2, 2, "variable")
        self.assertEqual(len(self.st.table["hello"]), 2)
        self.assertEqual(self.st.table["hello"][-1].token, t2)
        self.assertEqual(self.st.table["hello"][-1].depth, 2)

        t3 = Token("idt")
        t3.lexeme = "world"
        self.st.insert(t3, 2, "variable")
        self.assertEqual(len(self.st.table["world"]), 1)
        self.assertEqual(self.st.table["world"][-1].token, t3)
        self.assertEqual(self.st.table["world"][-1].depth, 2)

        self.st.delete(1)
        self.assertEqual(len(self.st.table["hello"]), 1)
        self.assertNotEqual(self.st.table["hello"][0].depth, 1)

        self.st.delete(2)
        self.assertEqual(len(self.st.table["hello"]), 0)
        self.assertEqual(len(self.st.table["world"]), 0)
