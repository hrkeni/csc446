#!/usr/bin/python
"""
Test suite for scanner
"""
from scanner import Scanner
import unittest


class TestScanner(unittest.TestCase):

    def setUp(self):
        self.scanner = Scanner()

    def test_known_token(self):
        self.scanner.data = "program ;"
        self.assertEqual(self.scanner.get_next_token(), "programt")
        self.assertEqual(self.scanner.get_next_token(), "semicolon")

    def test_unknown_token(self):
        self.scanner.data = "#"
        self.assertEqual(self.scanner.get_next_token(), "unknownt")
