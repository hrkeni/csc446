#!/usr/bin/python
"""
This module contains the main class definition for the parser.
"""

from scanner import Scanner
from scanner import Token
from symbol_table import SymbolTable
from code_gen import CodeGen
import logging
import pprint
logger = logging.getLogger('compiler.parser')
pp = pprint.PrettyPrinter()


class Parser(object):
    """
        This class implements a recursive descent parser for the subset of the
        PASCAL programming language we have defined. It implements the
        following grammar.

        PROG        -> programt idt ; DECLS SUB_PROGS COMP_STAT .
        DECLS       -> CONSTS VARS
        CONSTS      -> constt CONST_DECL | E
        CONST_DECL  -> idt relop numt ; CONST_DECL | E
        VARS        -> vart VAR_DECL | E
        VAR_DECL    -> ID_LIST : TYPE ; VAR_DECL | E
        ID_LIST     -> idt ID_LIST_NXT
        ID_LIST_NXT -> , idt ID_LIST_NXT | E
        TYPE        -> integert | realt | chart
        SUB_PROGS   -> SUB_DECL SUB_PROGS | E
        SUB_DECL    -> proceduret idt ARGS ; DECLS COMP_STAT ;
        ARGS        -> ( ARG_LIST ) | E
        ARG_LIST    -> MODE ID_LIST : TYPE MORE_ARGS
        MORE_ARGS   -> ; ARG_LIST | E
        MODE        -> vart | E
        COMP_STAT   -> begint STAT_LIST endt
        STAT_LIST   -> STATEMENT STAT_TAIL | E
        STAT_TAIL   -> ; STATEMENT STAT_TAIL | E
        STATEMENT   -> ASSIGN_STAT | IO_STAT
        ASSIGN_STAT -> idt assignop EXPR | PROC_CALL
        PROC_CALL   -> idt ( PARAMS )
        PARAMS      -> idt PARAMS_TAIL | numt PARAMS_TAIL | E
        PARAMS_TAIL -> , idt PARAMS_TAIL | , numt PARAMS_TAIL | E
        IO_STAT     -> IN_STAT | OUT_STAT
        IN_STAT     -> read(ID_LIST)
        OUT_STAT    -> write(WRT_LST) | writeln(WRT_LST)
        WRT_LST     -> WRITE_TOKEN | WRT_LST_TL
        WRT_LST_TL  -> , WRITE_TOKEN WRT_LST_TL | E
        WRITE_TOKEN -> idt | numt | literalt
        EXPR        -> RELATION
        RELATION    -> SIMPLE_EXPR
        SIMPLE_EXPR -> TERM MORE_TERM
        MORE_TERM   -> addop TERM MORE_TERM | E
        TERM        -> FACTOR MORE_FACTOR
        MORE_FACTOR -> mulop FACTOR MORE_FACTOR | E
        FACTOR      -> idt | numt | ( EXPR ) | not FACTOR | signop FACTOR

    """

    def parse(self, filename):
        """
            Start parsing given file.
            Instantiates a scanner.
        """
        self.scanner = Scanner(filename)
        outname = filename.split('/')[-1][:-4] + '.tac'
        self.outfile = open(outname, 'w')
        self.st = SymbolTable()
        self.token = self.scanner.get_next_token()  # initialize first token
        self.prog()  # start parsing
        logger.info("Three address code generated at: {0}".format(outname))
        self.outfile.close()
        gen = CodeGen()
        gen.generate(outname[:-4])
        logger.info("Compilation success!")

    def match(self, expected):
        """
            Matches expected token with actual token from scanner.
            Args:
                expected(str) - expected token

            Returns: expected == token
        """
        if expected == self.token:
            logger.debug("Matched: {0} Line Number: {1}"
                         .format(expected, self.token.linenumber))
        else:
            msg = ("Line: {0} Expected: {1} Found: {2}"
                   .format(self.token.linenumber, expected, self.token))
            raise ParseError(msg)
        self.token = self.scanner.get_next_token()

    def prog(self):
        """
            Implements grammar rule:
            PROG        -> programt idt ; DECLS SUB_PROGS COMP_STAT .
        """
        logger.debug("Rule PROG")
        depth = 1  # program starts at depth 1
        self.match("programt")
        token = self.token
        self.match("idt")
        self.current_proc = None
        self.match("semicolon")
        self.decls(depth)
        self.sub_progs(depth)
        self.emit("proc " + token.lexeme)
        self.comp_stat(depth)
        self.match("period")
        self.emit("endp " + token.lexeme + '\n')
        self.emit("start " + token.lexeme + '\n')
        self.match("eoft")
        logger.debug("End PROG. ST: {0}".format(self.st))

    def decls(self, depth):
        """
            Implements grammar rule:
                DECLS       -> CONSTS VARS
        """
        logger.debug("Rule DECLS")
        self.consts(depth)
        offset = 0
        offset = self.vars(depth, offset)
        return offset

    def consts(self, depth):
        """
            Implements grammar rule:
                CONSTS      -> constt CONST_DECL | E
        """
        logger.debug("Rule CONSTS")
        if self.token == "constt":
            self.match("constt")
            self.const_decl(depth)

    def const_decl(self, depth):
        """
            Implements grammar rule:
                CONST_DECL  -> idt relop numt ; CONST_DECL | E
        """
        logger.debug("Rule CONST_DECL")
        if self.token == "idt":
            token = self.token  # Token for idt
            self.match("idt")
            entry = self.st.insert(token, depth, "constant")
            self.match("relop")
            token = self.token
            self.match("numt")
            entry.value = token.value  # update entry with value
            self.match("semicolon")
            self.const_decl(depth)

    def vars(self, depth, offset):
        """
            Implements grammar rule:
                VARS        -> vart VAR_DECL | E
        """
        logger.debug("Rule VARS")
        if self.token == "vart":
            self.match("vart")
            offset = self.var_decl(depth, offset)
        return offset

    def var_decl(self, depth, offset):
        """
            Implements grammar rule:
                VAR_DECL    -> ID_LIST : TYPE ; VAR_DECL | E
        """
        logger.debug("Rule VAR_DECL")
        if self.token in ["idt", "comma"]:  # first(VAR_DECL)
            entries = self.id_list(depth)
            for e in entries:
                e.local = True
            self.match("colon")
            offset = self.type(entries, offset)
            self.match("semicolon")
            offset = self.var_decl(depth, offset)
        return offset

    def id_list(self, depth=None):
        """
            Implements rule:
                ID_LIST     -> idt ID_LIST_NXT
        """
        logger.debug("Rule ID_LIST")
        entries = []
        if depth:
            entries.append(self.st.insert(self.token, depth, "variable"))
        else:  # hack to reuse same code for decls and read stats
            self.check_declared(self.token, ["variable"])
            self.emit('rdi {0}'.format(self.token.lexeme))
        self.match("idt")
        self.id_list_nxt(entries, depth)
        return entries

    def id_list_nxt(self, entries, depth):
        """
            Implements rule:
                ID_LIST_NXT -> , idt ID_LIST_NXT | E
        """
        logger.debug("Rule ID_LIST_NXT")
        if self.token == "comma":
            self.match("comma")
            if depth:
                entries.append(self.st.insert(self.token, depth, "variable"))
            else:  # hack to reuse same code for decls and read stats
                self.check_declared(self.token, ["variable"])
                self.emit('rdi {0}'.format(self.token.lexeme))
            self.match("idt")
            self.id_list_nxt(entries, depth)

    def type(self, entries, offset):
        """
            Implements rule:
                TYPE        -> integert | realt | chart
        """
        logger.debug("Rule TYPE")
        if self.token == "integert":
            self.match("integert")
            for e in entries:
                e.vartype = "integer"
                e.offset = offset
                offset += 2
        elif self.token == "realt":
            self.match("realt")
            for e in entries:
                e.vartype = "real"
                e.offset = offset
                offset += 4
        elif self.token == "chart":
            self.match("chart")
            for e in entries:
                e.vartype = "char"
                e.offset = offset
                offset += 1
        else:
            msg = ("Line: {0} Expected one of 'integert', 'realt', 'chart' Found: {1})"
                   .format(self.token.linenumber, self.token))
            raise ParseError(msg)
        return offset

    def sub_progs(self, depth):
        """
            Implements rule:
                SUB_PROGS   -> SUB_DECL SUB_PROGS | E
        """
        logger.debug("Rule SUB_PROGS")
        if self.token == "proceduret":  # first(SUB_PROGS)
            self.sub_decls(depth)
            self.sub_progs(depth)

    def sub_decls(self, depth):
        """
            Implements rule:
                SUB_DECL    -> proceduret idt ARGS ; DECLS COMP_STAT ;
        """
        logger.debug("Rule SUB_DECL")
        self.match("proceduret")
        token = self.token
        self.match("idt")
        self.current_proc = entry = self.st.insert(token, depth, "procedure")
        self.emit('proc ' + token.lexeme)
        params, size_param = self.args(depth + 1)
        l = []
        for param in params:
            d = {}
            d['lexeme'] = param.token.lexeme
            d['pass_by'] = param.pass_by
            d['type'] = param.vartype  # type refers to vartype
            l.append(d)
        entry.param_list = l
        entry.size_param = size_param
        self.match("semicolon")
        entry.size_local = self.decls(depth + 1)
        self.comp_stat(depth + 1)
        self.emit('endp ' + token.lexeme + '\n')
        self.match("semicolon")
        logger.debug("Entries at depth {0}:\n {1}"
                     .format(depth + 1,
                             pp.pformat(self.st.all_at_depth(depth + 1))))
        self.st.delete(depth + 1)

    def args(self, depth):
        """
            Implements rule:
                ARGS        -> ( ARG_LIST ) | E
        """
        logger.debug("Rule ARGS")
        entries = []
        offset = 0
        if self.token == "lparen":
            self.match("lparen")
            offset = self.arg_list(entries, depth, offset)
            self.match("rparen")
        return (entries, offset)

    def arg_list(self, params, depth, offset):
        """
            Implements rule:
                ARG_LIST    -> MODE ID_LIST : TYPE MORE_ARGS
        """
        logger.debug("Rule ARG_LIST")
        pass_by = self.mode()
        entries = self.id_list(depth)
        params.extend(entries)  # add entries to the list of params
        for e in entries:
            e.param = True
            e.pass_by = pass_by
        self.match("colon")
        # offset = self.type(entries[::-1], offset)
        offset = self.type(entries, offset)
        offset = self.more_args(params, depth, offset)
        return offset

    def more_args(self, params, depth, offset):
        """
            Implements rule:
                MORE_ARGS   -> ; ARG_LIST | E
        """
        logger.debug("Rule MORE_ARGS")
        if self.token == "semicolon":
            self.match("semicolon")
            offset = self.arg_list(params, depth, offset)
        return offset

    def mode(self):
        """
            Implements rule:
                MODE        -> vart | E

            Returns
                pass_by(str) : 'val' or 'ref'
        """
        logger.debug("Rule MODE")
        if self.token == "vart":
            self.match("vart")
            return "ref"
        return "val"

    def comp_stat(self, depth):
        """
            Implements rule:
                COMP_STAT   -> begint STAT_LIST endt
        """
        logger.debug("Rule COMP_STAT")
        self.match("begint")
        self.stat_list(depth)
        self.match("endt")

    def stat_list(self, depth):
        """
            Implements rule:
                STAT_LIST   -> STATEMENT STAT_TAIL | E
        """
        logger.debug("Rule STAT_LIST")
        if self.token == "idt":  # first(STAT_LIST)
            self.statement(depth)
            self.stat_tail(depth)

    def stat_tail(self, depth):
        """
            Implements rule:
            STAT_TAIL   -> ; STATEMENT STAT_TAIL | E
        """
        logger.debug("Rule STAT_TAIL")
        if self.token == "semicolon":
            self.match("semicolon")
            self.statement(depth)
            self.stat_tail(depth)

    def statement(self, depth):
        """
            Implements rule:
            STATEMENT   -> ASSIGN_STAT | IO_STAT
        """
        logger.debug("Rule STATEMENT")
        if self.token == "idt":  # first(STATEMENT)
            self.assign_stat(depth)
        else:
            self.io_stat()

    def assign_stat(self, depth):
        """
            Implements rule:
            ASSIGN_STAT -> idt assignop EXPR | PROC_CALL
        """
        logger.debug("Rule ASSIGN_STAT")
        entry = self.st.lookup(self.token.lexeme)

        if entry == None:
            raise ParseError("Line: {0}. '{1}' not in symbol table."
                             .format(self.token.linenumber, self.token.lexeme))
        if entry.type == "procedure":
            self.proc_call(entry)
        elif entry.type == "variable":
            self.match("idt")
            self.match("assignop")
            eplace = self.expr(depth, None)
            code = ''
            if entry.depth > 1:
                if hasattr(entry, 'pass_by') and entry.pass_by == 'ref':
                    code = '@'
                if entry.local:
                    code += '_bp-' + str(entry.offset + 2)
                else:
                    code += '_bp+' + str(entry.offset + 4)
            else:
                code = entry.token.lexeme
            code += ' = '
            if type(eplace) is Token:
                code += eplace.lexeme
            else:
                if eplace.type == 'constant':
                    code += str(eplace.value)
                else:
                    if eplace.depth > 1:
                        if hasattr(eplace, 'pass_by') and eplace.pass_by == 'ref':
                            code += '@'
                        if eplace.local:
                            code += '_bp-' + str(eplace.offset + 2)
                        else:
                            code += '_bp+' + str(eplace.offset + 4)
                    else:
                        code += eplace.token.lexeme
            self.emit(code)
        else:
            raise ParseError("Line: {0}. '{1}' not valid identifier."
                             .format(self.token.linenumber, self.token.lexeme))

    def proc_call(self, proc_entry):
        """
            Implements rule:
            PROC_CALL   -> idt ( PARAMS )
        """
        logger.debug("Rule PROC_CALL")
        # already checked existence and idt type in assign_stat
        self.match("idt")
        self.match("lparen")
        self.params(proc_entry)
        self.emit('call ' + proc_entry.token.lexeme)
        self.match("rparen")

    def params(self, proc_entry):
        """
            Implements rule:
            PARAMS      -> idt PARAMS_TAIL | numt PARAMS_TAIL | E
        """
        logger.debug("Rule PARAMS")
        formals = proc_entry.param_list[:]
        if self.token == "idt":
            if len(formals) == 0:
                raise ParseError("{0} parameters expected when calling procedure {1}"
                                 .format(len(proc_entry.param_list), proc_entry.token.lexeme))
            # make sure idt exists in st
            self.check_declared(self.token, ["constant", "variable"])
            # check actual params with formal params
            formal = formals.pop(0)
            code = 'push '
            entry = self.st.lookup(self.token.lexeme)
            if formal['pass_by'] == "ref":
                if entry.type == 'constant':
                    raise ParseError('Line {0}: Cannot pass constant by reference'.format(self.token.linenumber))
                if entry.depth > 1:
                    if entry.local:
                        code += "@_bp-" + str(entry.offset + 2)
                    else:
                        code += "@_bp+" + str(entry.offset + 4)
                else:
                    code += "@" + self.token.lexeme
            else:
                if entry.type == 'constant':
                    code += entry.value
                elif entry.depth > 1:  # it is a var entry
                    if entry.local:
                        code += "_bp-" + str(entry.offset + 2)
                    else:
                        code += "_bp+" + str(entry.offset + 4)
                else:
                    code += self.token.lexeme
            self.emit(code)
            self.match("idt")
            self.params_tail(proc_entry, formals)
        elif self.token == "numt":
            if len(formals) == 0:
                raise ParseError("{0} parameters expected when calling procedure {1}"
                                 .format(len(proc_entry.param_list), proc_entry.token.lexeme))
            # check actual params with formal params
            formal = formals.pop(0)
            if formal['pass_by'] == 'ref':
                raise ParseError('Line {0}: cannot pass literal by reference'
                                 .format(self.token.linenumber))
            self.emit('push ' + self.token.lexeme)
            self.match("numt")
            self.params_tail(proc_entry, formals)
        # we pop a formal param from the list for every one we matched
        if len(formals) != 0:
            raise ParseError("{0} parameters expected when calling procedure {1}"
                             .format(len(proc_entry.param_list), proc_entry.token.lexeme))

    def params_tail(self, proc_entry, formals):
        """
            Implements rule:
            PARAMS_TAIL -> , idt PARAMS_TAIL | , numt PARAMS_TAIL | E
        """
        logger.debug("Rule PARAMS_TAIL")
        if self.token == "comma":
            self.match("comma")
            if self.token == "idt":
                if len(formals) == 0:
                    raise ParseError("{0} parameters expected when calling procedure {1}"
                                     .format(len(proc_entry.param_list), proc_entry.token.lexeme))
                self.check_declared(self.token, ["constant", "variable"])
                # check actual params with formal params
                formal = formals.pop(0)
                code = 'push '
                entry = self.st.lookup(self.token.lexeme)
                if formal['pass_by'] == "ref":
                    if entry.type == 'constant':
                        raise ParseError('Line {0}: Cannot pass constant by reference'.format(self.token.linenumber))
                    if entry.depth > 1:
                        if entry.local:
                            code += "@_bp-" + str(entry.offset + 2)
                        else:
                            code += "@_bp+" + str(entry.offset + 4)
                    else:
                        code += "@" + self.token.lexeme
                else:
                    if entry.type == 'constant':
                        code += entry.value
                    elif entry.depth > 1:  # it is a var entry
                        if entry.local:
                            code += "_bp-" + str(entry.offset + 2)
                        else:
                            code += "_bp+" + str(entry.offset + 4)
                    else:
                        code += self.token.lexeme
                self.emit(code)
                self.match("idt")
            elif self.token == "numt":
                if len(formals) == 0:
                    raise ParseError("{0} parameters expected when calling procedure {1}"
                                     .format(len(proc_entry.param_list), proc_entry.token.lexeme))
                # check actual params with formal params
                formal = formals.pop(0)
                if formal['pass_by'] == 'ref':
                    raise ParseError('Line {0}: cannot pass literal by reference'
                                     .format(self.token.linenumber))
                self.emit('push ' + self.token.lexeme)
                self.match("numt")
            else:
                raise ParseError("Invalid parameters on line: {0}"
                                 .format(self.token.linenumber))
            self.params_tail(proc_entry, formals)

    def io_stat(self):
        """
            Implements rule:
            IO_STAT     -> IN_STAT | OUT_STAT
        """
        logger.debug("Rule IO_STAT")
        if self.token == "readt":
            self.in_stat()
        elif self.token == "writet":
            self.out_stat()
        else:
            raise ParseError("Line {0}: expected read or write"
                             .format(self.token.linenumber))

    def in_stat(self):
        """
            Implements rule:
            IN_STAT     -> read(ID_LIST)
        """
        logger.debug("Rule IN_STAT")
        self.match('readt')
        self.match('lparen')
        self.id_list()
        self.match('rparen')

    def out_stat(self):
        """
            Implements rule:
            OUT_STAT    -> write(WRT_LST) | writeln(WRT_LST)
        """
        logger.debug("Rule OUT_STAT")
        wrt_tk = self.token
        self.match('writet')
        self.match('lparen')
        self.write_list()
        if wrt_tk.lexeme == "writeln":
            self.emit('wrln')
        self.match('rparen')

    def write_list(self):
        """
            Implements rule:
            WRT_LST     -> WRITE_TOKEN | WRT_LST_TL
        """
        logger.debug("Rule WRT_LST")
        self.write_token()
        self.write_list_tail()

    def write_list_tail(self):
        """
            Implements rule:
            WRT_LST_TL  -> , WRITE_TOKEN WRT_LST_TL | E
        """
        logger.debug("Rule WRT_LST_TL")
        if self.token == "comma":
            self.write_token()
            self.write_list_tail()

    def write_token(self):
        """
            Implements rule:
            WRITE_TOKEN -> idt | numt | literalt
        """
        logger.debug("Rule WRT_LST_TL")
        if self.token == "idt":
            self.check_declared(self.token, ["constant", "variable"])
            self.emit('wri {0}'.format(self.token.lexeme))
            self.match('idt')
        elif self.token == 'numt':
            self.emit('wri {0}'.format(self.token.lexeme))
            self.match('numt')
        elif self.token == 'literalt':
            self.st.literals.append(self.token.literal)
            self.emit('wrs _S' + str(len(self.st.literals) - 1))
            self.match('literalt')

    def expr(self, depth, eplace):
        """
            Implements rule:
            EXPR        -> RELATION
        """
        logger.debug("Rule EXPR")
        return self.relation(depth, eplace)

    def relation(self, depth, eplace):
        """
            Implements rule:
            RELATION    -> SIMPLE_EXPR
        """
        logger.debug("Rule RELATION")
        return self.simple_expr(depth, eplace)

    def simple_expr(self, depth, eplace):
        """
            Implements rule:
            SIMPLE_EXPR -> TERM MORE_TERM
        """
        logger.debug("Rule SIMPLE_EXPR")
        tplace = self.term(depth, eplace)
        return self.more_term(depth, tplace)

    def more_term(self, depth, tplace):
        """
            Implements rule:
            MORE_TERM   -> addop TERM MORE_TERM | E
        """
        logger.debug("Rule MORE_TERM")
        if self.token == "addop":
            temp = self.st.gentemp(depth, self.current_proc)
            code = None
            if temp.depth > 1:
                code = "_bp-" + str(temp.offset + 2)
            else:
                code = temp.token.lexeme
            code += ' = '.format(temp.token.lexeme)
            if type(tplace) == Token:
                code += str(tplace.lexeme)
            else:
                if tplace.type == 'constant':
                    code += str(tplace.value)
                else:
                    if hasattr(tplace, 'pass_by') and tplace.pass_by == 'ref':
                        code += '@'
                    if tplace.depth > 1:
                        if tplace.local:
                            code += "_bp-" + str(tplace.offset + 2)
                        else:
                            code += "_bp+" + str(tplace.offset + 4)
                    else:
                        code += tplace.token.lexeme
            code = code + ' {0} '.format(self.token.lexeme)  # concat the op
            self.match("addop")
            rplace = self.term(depth, tplace)
            if type(rplace) is Token:
                code += rplace.lexeme
            else:
                if rplace.type == 'constant':
                    code += str(rplace.value)
                else:
                    if hasattr(rplace, 'pass_by') and rplace.pass_by == 'ref':
                        code += '@'
                    if rplace.depth > 1:
                        if rplace.local:
                            code += "_bp-" + str(rplace.offset + 2)
                        else:
                            code += "_bp+" + str(rplace.offset + 4)
                    else:
                        code += rplace.token.lexeme
            self.emit(code)
            return self.more_term(depth, temp)
        return tplace

    def term(self, depth, tplace):
        """
            Implements rule:
            TERM        -> FACTOR MORE_FACTOR
        """
        logger.debug("Rule TERM")
        rplace = self.factor(depth, tplace)
        return self.more_factor(depth, rplace)

    def more_factor(self, depth, fplace):
        """
            Implements rule:
            MORE_FACTOR -> mulop FACTOR MORE_FACTOR | E
        """
        logger.debug("Rule MORE_FACTOR")
        if self.token == "mulop":
            temp = self.st.gentemp(depth, self.current_proc)
            code = None
            if temp.depth > 1:
                code = "_bp-" + str(temp.offset + 2)
            else:
                code = temp.token.lexeme
            code += ' = '.format(temp.token.lexeme)
            if type(fplace) == Token:
                code += str(fplace.lexeme)
            else:
                if fplace.type == 'constant':
                    code += str(fplace.value)
                else:
                    if fplace.depth > 1:
                        if hasattr(fplace, 'pass_by') and fplace.pass_by == 'ref':
                            code += '@'
                        if fplace.local:
                            code += "_bp-" + str(fplace.offset + 2)
                        else:
                            code += "_bp+" + str(fplace.offset + 4)
                    else:
                        code += fplace.token.lexeme
            code = code + ' {0} '.format(self.token.lexeme)
            self.match("mulop")
            rplace = self.factor(depth, fplace)
            if type(rplace) is Token:
                code += rplace.lexeme
            else:
                if rplace.type == 'constant':
                    code += str(rplace.value)
                else:
                    if rplace.depth > 1:
                        if hasattr(rplace, 'pass_by') and rplace.pass_by == 'ref':
                            code += '@'
                        if rplace.local:
                            code += "_bp-" + str(rplace.offset + 2)
                        else:
                            code += "_bp+" + str(rplace.offset + 4)
                    else:
                        code += rplace.token.lexeme
            self.emit(code)
            return self.more_factor(depth, temp)
        return fplace

    def factor(self, depth, fplace):
        """
            Implements rule:
            FACTOR      -> idt | numt | ( EXPR ) | not FACTOR | signop FACTOR
        """
        logger.debug("Rule FACTOR")
        token = self.token
        if token == "idt":
            self.check_declared(self.token, ["constant", "variable"])
            self.match("idt")
            return self.st.lookup(token.lexeme)
        elif token == "numt":
            self.match("numt")
            return token
        elif token == "lparen":
            self.match("lparen")
            rplace = self.expr(depth, None)
            self.match("rparen")
            return rplace
        elif token == "nott":
            temp = self.st.gentemp(depth, self.current_proc)
            code = ''
            if temp.depth > 1:
                code = "_bp-" + str(temp.offset + 2)
            else:
                code = temp.token.lexeme
            code += ' = {0} '.format(self.token.lexeme)
            self.match("nott")
            rplace = self.factor(depth, fplace)
            if type(rplace) == Token or rplace.type == 'constant':
                code += ' {0}'.format(rplace.value)
            else:
                if rplace.depth > 1:
                    if hasattr(rplace, 'pass_by') and rplace.pass_by == 'ref':
                        code += '@'
                    if rplace.local:
                        code += "_bp-" + str(rplace.offset + 2)
                    else:
                        code += "_bp+" + str(rplace.offset + 4)
                else:
                    code += rplace.token.lexeme
            self.emit(code)
            return temp
        elif token.symbol == "-":
            temp = self.st.gentemp(depth, self.current_proc)
            code = ''
            code = ''
            if temp.depth > 1:
                code = "_bp-" + str(temp.offset + 2)
            else:
                code = temp.token.lexeme
            code += ' = {0}'.format(self.token.lexeme)
            self.match("addop")  # signop is a subset of addop
            rplace = self.factor(depth, fplace)
            if type(rplace) == Token or rplace.type == 'constant':
                code += str(rplace.value)
            else:
                if rplace.depth > 1:
                    if hasattr(rplace, 'pass_by') and rplace.pass_by == 'ref':
                        code += '@'
                    if rplace.local:
                        code += "_bp-" + str(rplace.offset + 2)
                    else:
                        code += "_bp+" + str(rplace.offset + 4)
                else:
                    code += rplace.token.lexeme
            self.emit(code)
            return temp
        else:
            raise ParseError("Expected one of [idt, numt, lapren, not, signop] at line number {0}".format(token.linenumber))
        return token

    def check_declared(self, token, allowed):
        """
            Checks if passed token is an identifier in the symbol table

            Args:
                token(Token)      : Token to be checked
                allowed(list:str) : List of idt types that are allowed

            Raises:
                ParseError: if lookup fail or unexpectedd type.
        """
        entry = self.st.lookup(token.lexeme)
        if entry == None or entry.type not in allowed:
            raise ParseError("Error in line: {0}. '{1}' not in symbol table."
                             .format(token.linenumber, token.lexeme))
        return entry

    def emit(self, code):
        """
            Writes to three address output.
        """
        logger.debug(code)
        self.outfile.write(code + '\n')


class ParseError(Exception):
    pass
