#!/usr/bin/python
"""
This module converts the three address code into 8086 assembly code
"""
import string
import logging
logger = logging.getLogger('compiler.code_gen')


class CodeGen(object):
    """
        This class reads a given .tac file and generates 8086 .asm file
    """
    def __init__(self):
        """
            Class constructor
        """
        pass

    def generate(self, filename):
        """
            Reads given file, generates output .asm
        """
        logger.debug("Generating output from {0}.tac".format(filename))
        infile = open(filename + '.tac')
        self.outfile = open(filename + '.asm', 'w')
        self.gen_head()
        for line in infile:
            if line.startswith('proc '):
                code = self.proc_start(line)
            elif line.startswith('endp '):
                code = self.proc_end(line)
            elif line.startswith('start '):
                code = self.gen_main(line)
            else:
                code = line
            self.emit(code)
        self.outfile.close()
        logger.info("Successfully generated {0}.asm".format(filename))

    def gen_main(self, line):
        name = line[6:]
        code = 'main PROC\n'
        code += 'mov ax, @data\n'
        code += 'mov ds, ax\n'
        code += 'call ' + name + '\n'
        code += 'mov ah, 04ch\n'
        code += 'int 21h\n'
        code += 'main ENDP\n'
        code += 'END main\n'
        return code

    def gen_head(self):
        code = ".model small\n"
        code += ".stack 100h\n"
        code += ".data\n"
        code += ".code\n"
        code += "include io.asm\n"
        self.emit(code)

    def proc_start(self, line):
        name = line[5:]
        code = name[:-1] + ' PROC\n'
        code += 'push bp\n'
        code += 'mov bp, sp\n'
        code += 'sub sp, size_local'  # TODO
        return code

    def proc_end(self, line):
        name = line[5:]
        code = 'add sp, size_local\n'  # TODO
        code += 'push bp\n'
        code += 'ret size_param\n'  # TODO
        code += name[:-1] + ' ENDP'
        return code

    def emit(self, code):
        """
            Writes to three address output.
        """
        logger.debug(code)
        self.outfile.write(code + '\n')
