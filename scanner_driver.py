#!/usr/bin/python
"""
This module is the main driver for the lexical analyser.
It reads command line arguments, instantiates the scanner class and passes on
the filename of the file to be analysed to it.
It then repeatedly calls get_next_token until the input file reaches eof.

Usage: python main.py <filename>
"""

import sys, logging
from src.scanner import Scanner
# create logger
logger = logging.getLogger('compiler')
logger.setLevel(logging.INFO)
# stream logger
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


if len(sys.argv) != 2:
    print "Wrong number of arguments.\nUsage: python main.py <filename>"
    sys.exit(1)
filename = sys.argv[1]
scanner = Scanner(filename)
printcounter = 0
while True:
    if printcounter == 20:
        raw_input("\nPress enter key to continue...\n")
        printcounter = 0
    token = scanner.get_next_token()
    token.display_output()
    if token == "eoft":
        print "\nEnd of file"
        exit(0)
    printcounter += 1
