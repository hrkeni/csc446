# A simple PASCAL compiler for CSC446
# Assignment 7 - Intermedia Code Generation
## Instructor name: Dr. Hamer

# Author
- Name: Harshith Keni
- Student ID: 7327961
- Email: harshith.keni@jacks.sdstate.edu

# Notes:
- Usage: python main.py <filename>
- Useful log messages (including required output) is logged to the console
- More verbose logging is available in compiler.log
