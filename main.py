#!/usr/bin/python
"""
This module is the main driver for the parser.
It reads command line arguments, instantiates the parser class and passes on
the filename of the file to be analysed to it.

Usage: python main.py <filename>
"""

import sys
from src.parser import Parser, ParseError
from src.symbol_table import DuplicateInsertion
import logging
# create logger
logger = logging.getLogger('compiler')
logger.setLevel(logging.DEBUG)
# stream logger
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# file logger
fh = logging.FileHandler("compiler.log")
fh.setLevel(logging.DEBUG)
fs = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
formatter = logging.Formatter(fs)
fh.setFormatter(formatter)
logger.addHandler(fh)

if len(sys.argv) != 2:
    logger.error("Wrong number of arguments\nUsage: python main.py <filename>")
    sys.exit(1)
filename = sys.argv[1]
parser = Parser()
try:
    parser.parse(filename)
except ParseError, e:
    logger.error("Parse error: {0}".format(e))
except DuplicateInsertion, e:
    logger.error("Symbol table error: {0}".format(e))
# except Exception, e:
#     logger.error("Error: {0}".format(e))
