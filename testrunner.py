"""
Unit test runner
"""

from src import TestScanner
from src import TestSymbolTable
import unittest
import logging
# create logger
logger = logging.getLogger('compiler')
logger.setLevel(logging.DEBUG)

# file logger
fh = logging.FileHandler("testlog.log")
fh.setLevel(logging.DEBUG)
fs = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
formatter = logging.Formatter(fs)
fh.setFormatter(formatter)
logger.addHandler(fh)

if __name__ == "__main__":
    unittest.main()
